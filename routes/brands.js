const express = require('express');
const router = express.Router();
const models = require('../models');
const form = require('express-form');
const field = form.field;

// Formulaire de création
router.get('/create', function(req, res, next) {
	res.render('brands/create');
});

function createForm() {
	return form(
		field("name").trim().required(),
	);
}

// Formulaire d'édition
router.all('/:id/edit', createForm(), function(req, res, next) {
	models
		.Brand
		.findById(req.params.id)
		.then(brand => {
			if (req.method == "POST" && req.form.isValid) {
				brand
					.update({
						name: req.form.name
					}).then(() => {
		    			res.redirect(`/brands/${brand.id}/edit`);
					})
				;
		    }
		    if(req.method == "GET") {
				res.render('brands/edit', {
					brand: brand,
				});
		    }
		})
	;
});

router.post('/create', createForm(), function(req, res){
	if (req.form.isValid) {
		models
			.Brand
			.create({
				name: req.form.name
			}).then(() => {
    			res.redirect('/brands/create');
			})
		;

    } else {
    	res.redirect('/brands/create');
    }

});

module.exports = router;
