'use strict';
module.exports = (sequelize, DataTypes) => {
  const Car = sequelize.define('Car', {
    name: DataTypes.STRING,
    engine: DataTypes.STRING,
    color: DataTypes.STRING,
    model: DataTypes.STRING,
    fuel: DataTypes.STRING
  }, {});
  Car.associate = function(models) {
    models.Car.belongsTo(models.Brand,{
      as:'brand',
      onDelete:"CASCADE",
      foreignKey:{
        allowNull: false
      }
    })
  };
  return Car;
};
